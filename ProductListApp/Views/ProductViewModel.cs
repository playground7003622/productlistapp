﻿using ProductListApp.Models;

namespace ProductListApp.Views
{
    public class ProductViewModel
    {
        public List<ProductModel> Products { get; set; }
    }

}
