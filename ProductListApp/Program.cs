using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ProductListApp.Data;
using ProductListApp.DB;
using ProductListApp.Models;
using ProductListApp.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase("MyInMemoryDb"));
builder.Services.AddControllersWithViews();
builder.Services.AddScoped<ProductService>();

var app = builder.Build();

// Initialize the database with mock data
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    try
    {
        // Get the database context
        var dbContext = services.GetRequiredService<ApplicationDbContext>();

        // Seed the database
        SeedData(dbContext);
    }
    catch (Exception ex)
    {
        var logger = services.GetRequiredService<ILogger<Program>>();
        logger.LogError(ex, "An error occurred seeding the database.");
    }
}

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

// Seed data method
static void SeedData(ApplicationDbContext context)
{
    context.Products.Add(new ProductModel
    {
        Id = 1,
        Name = "Tailored Jeans",
        Description = "Super slim and comfy lorem ipsum lorem jeansum.",
        Price = 19.99m,
        ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7"
    });
    context.Products.Add(new ProductModel
    {
        Id = 2,
        Name = "Tailored Jeans",
        Description = "Super slim and comfy lorem ipsum lorem jeansum.",
        Price = 19.99m,
        ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7"
    });

    context.SaveChanges();
}



// using ProductListApp.Data;
// using ProductListApp.DB;

// var builder = WebApplication.CreateBuilder(args);

// // Add services to the container.
// builder.Services.AddControllersWithViews();

// // Initialize the database with mock data
// using (var scope = builder.Services.BuildServiceProvider().CreateScope())
// {
//     var services = scope.ServiceProvider;
//     try
//     {
//         // Get the database context
//         var dbContext = services.GetRequiredService<ApplicationDbContext>();

//         // Call the DataSeeder.Initialize method with the dbContext
//         DataSeeder.Initialize(dbContext);
//     }
//     catch (Exception ex)
//     {
//         var logger = services.GetRequiredService<ILogger<Program>>();
//         logger.LogError(ex, "An error occurred seeding the database.");
//     }
// }

// var app = builder.Build();

// // Configure the HTTP request pipeline.
// if (!app.Environment.IsDevelopment())
// {
//     app.UseExceptionHandler("/Home/Error");
//     // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
//     app.UseHsts();
// }

// app.UseHttpsRedirection();
// app.UseStaticFiles();

// app.UseRouting();

// app.UseAuthorization();

// app.MapControllerRoute(
//     name: "default",
//     pattern: "{controller=Home}/{action=Index}/{id?}");

// app.Run();