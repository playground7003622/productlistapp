﻿using ProductListApp.Data;
using ProductListApp.Models;

namespace ProductListApp.Services
{
    public class ProductService
    {
        private readonly ApplicationDbContext _context;

        public ProductService(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<ProductModel> GetAllProducts()
        {
            return _context.Products.ToList();
        }
    }
}