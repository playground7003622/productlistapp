﻿using Microsoft.EntityFrameworkCore;
using ProductListApp.Data;
using ProductListApp.Models;
using ProductListApp.Services;

namespace ProductListApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase("MyInMemoryDb"));
            services.AddScoped<ProductService>(); // Add your service
            services.AddControllersWithViews();
        }

        public static void SeedData(ApplicationDbContext context)
        {
            context.Products.Add(new ProductModel { Id = 1, Name = "Tailored Jeans", Description = "Super slim and comfy lorem ipsum lorem jeansum.", Price = 19.99m, ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7" });
            context.Products.Add(new ProductModel { Id = 2, Name = "Tailored Jeans", Description = "Super slim and comfy lorem ipsum lorem jeansum.", Price = 19.99m, ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7" });

            context.SaveChanges();
        }
    }
}