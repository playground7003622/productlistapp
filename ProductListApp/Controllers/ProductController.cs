﻿using Microsoft.AspNetCore.Mvc;
using ProductListApp.Data;
using ProductListApp.Services;
using ProductListApp.Views;

namespace ProductListApp.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ProductService _productService;

        public ProductsController(ApplicationDbContext context, ProductService productService)
        {
            _context = context;
            _productService = productService;
        }

        //public IActionResult Index()
        //{
        //    var products = _productService.GetAllProducts(); // Assuming you have a ProductService
        //    return View(products);
        //}

        public IActionResult Index()
        {
            var products = _productService.GetAllProducts();
            var viewModel = new ProductViewModel { Products = products };
            return View(viewModel);
        }
    }
}