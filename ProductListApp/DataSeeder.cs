﻿using ProductListApp.Data;
using ProductListApp.Models;

namespace ProductListApp.DB
{
    public static class DataSeeder
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            // Check if any products already exist.
            if (context.Products.Any())
            {
                return;   // DB has been seeded
            }

            var products = new List<ProductModel>
            {
                new ProductModel { Name = "Product1", Description = "Description1", Price = 10m, ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7"},
                new ProductModel { Name = "Product2", Description = "Description2", Price = 20m, ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7" },
                // Add more mock products as needed
            };

            context.Products.AddRange(products);
            context.SaveChanges();
        }
    }
}

//using Microsoft.EntityFrameworkCore;
//using ProductListApp.DB;
//using ProductListApp.Models;

//namespace ProductListApp
//{
//    public static class DataSeeder
//    {
//        public static void Initialize(IServiceProvider serviceProvider)
//        {
//            using (var context = new AppDbContext(
//                serviceProvider.GetRequiredService<DbContextOptions<AppDbContext>>()))
//            {
//                if (context.Products.Any())
//                {
//                    return;   // Data was already seeded
//                }

//                context.Products.AddRange(
//                    new Product
//                    {
//                        Name = "Product 1",
//                        Description = "Description for product 1",
//                        Price = 10.0M
//                    },
//                    new Product
//                    {
//                        Name = "Product 2",
//                        Description = "Description for product 2",
//                        Price = 20.0M
//                    }
//                );

//                context.SaveChanges();
//            }
//        }
//    }
//}