﻿using Microsoft.EntityFrameworkCore;
using ProductListApp.Models;

namespace ProductListApp.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ProductModel> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Configure other model-related settings here (if needed).

            // Seed initial data
            modelBuilder.Entity<ProductModel>().HasData(
                new ProductModel { Id = 1, Name = "Tailored Jeans", Description = "Super slim and comfy lorem ipsum lorem jeansum.", Price = 19.99M, ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7" },
                new ProductModel { Id = 2, Name = "Classic Jeans", Description = "Classic fit and durable jeans.", Price = 29.99M, ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7" }
            );
        }
    }
}

//using Microsoft.EntityFrameworkCore;
//using ProductListApp.Models;

//namespace ProductListApp.Data
//{
//    public class ApplicationDbContext : DbContext
//    {
//        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
//            : base(options)
//        {
//        }

//        public DbSet<Product> Products { get; set; }

//protected override void OnModelCreating(ModelBuilder modelBuilder)
//{
//    base.OnModelCreating(modelBuilder);
//    modelBuilder.Entity<Product>().HasData(
//        new Product { Id = 1, Name = "Tailored Jeans", Description = "Super slim and comfy lorem ipsum lorem jeansum.", Price = 19.99M, ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7" },
//        new Product { Id = 2, Name = "Classic Jeans", Description = "Classic fit and durable jeans.", Price = 29.99M, ImageUrl = "https://th.bing.com/th/id/OIP.ZoNBIQ266-O5pf8XOvJN6AHaJF?w=204&h=250&c=7&r=0&o=5&pid=1.7" }
//        // Add more products as needed
//    );
//}
//    }
//}